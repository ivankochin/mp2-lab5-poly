﻿# Отчёт 

## по самостоятельной работе №5 по дисциплине "Алгоритмы и Структуры Данных"

# тема:  "Полиномы"


## Требования к лабораторной работе

В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:

 - ввод полинома;
 - организация хранения полинома;
 - удаление введенного ранее полинома;
 - копирование полинома;
 - сложение двух полиномов;
 - вычисление значения полинома при заданных значениях переменных;
 - вывод.

Предполагается, что в качестве структуры хранения будут использоваться списки. В качестве дополнительной цели в лабораторной работе ставится также задача разработки некоторого общего представления списков и операций по их обработке. В числе операций над списками должны быть реализованы следующие действия:

 - поддержка понятия текущего звена;
 - вставка звеньев в начало, после текущей позиции и в конец списков;
 - удаление звеньев в начале и в текущей позиции списков;
 - организация последовательного доступа к звеньям списка (итератор).

В ходе выполнения лабораторной работы должно быть выполнено сопоставление разработанных средств работы со списками с возможностями работы со списками в библиотеке STL.

## Условия и ограничения

При выполнении лабораторной работы можно использовать следующие основные предположения:

 1. Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных.
 2. Степени переменных полиномов не могут превышать значения 9, т.е. 0 <= i, j, k <= 9.
 3. Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).

## Структуры хранения полиномов

Для представления полиномов могут быть выбраны различные структуры хранения. Критериями выбора структуры хранения являются размер требуемой памяти и сложность (трудоемкость) реализации операций над полиномами.

Возможный вариант структуры хранения – использование массивов (в случае полиномов от трех переменных – трехмерная матрица коэффициентов полинома). Такой способ обеспечивает простую реализацию операций над полиномами, но он не эффективен в части объема требуемой памяти. Так, при сделанных допущениях для хранения одного полинома в массиве потребуется порядка 8000 байт памяти – при этом в памяти будут храниться в основном параметры мономов с нулевыми коэффициентами.

Разработка более эффективной структуры хранения должна быть выполнена с учетом следующих рекомендаций:

 - в структуре хранения должны храниться данные только для мономов с ненулевыми коэффициентами; 
 - порядок размещения данных в структуре хранения должен обеспечивать возможность быстрого поиска мономов с заданными свойствами (например, для приведения подобных).

Так как степени переменных мономов ограничены и принимают значения от 0 до 9, то можно использовать ОДНО число, которое будет принимать значения от 0 до 999. Причем, если A, B и C - степени, то это число равно ABC = A*100+B*10+C. 
	
Данное соответствие является взаимно-однозначным. Обратное соответствие определяется при помощи выражений
A=E(ABC%100),  B=E(ABC-A*100)%10,  C=ABC-A*100-B*10.

Наиболее эффективным способом организации структуры хранения полиномов являются линейный (односвязный) список. Тем самым, в рамках лабораторной работы появляется подзадача – разработка структуры хранения в виде линейных списков. 

## Алгоритмы

### Списки

Для работы со списками реализуются следующие операции:

 - методы получения параметров состояния списка (проверка на пустоту, получение текущего количества звеньев);
 - метод доступа к значению первого, текущего или последнего звена;
 - методы навигации по списку (итератор);
 - методы вставки перед первым, после текущего и последнего звеньев;
 - методы удаления первого и текущего звена.

### Полиномы

Для работы с полиномами реализуются следующие операции:

 - конструкторы инициализации и копирования;
 - метод присваивания;
 - метод сложения полиномов.

## Структура

С учетом всех перечисленных ранее требований может быть предложен следующий состав классов и отношения между этими классами:

 - класс TDatValue для определения класса объектов-значений списка (абстрактный класс);
 - класс TMonom для определения объектов-значений параметров монома;
 - класс TRootLink для определения звеньев (элементов) списка (абстрактный класс);
 - класс TDatLink для определения звеньев (элементов) списка с указателем на объект-значение;
 - класс TDatList для определения линейных списков;
 - класс THeadRing для определения циклических списков с заголовком;
 - класс TPolinom для определения полиномов.
На рисунке ниже показаны также отношения между классами: обычными стрелками показаны отношения наследования (базовый класс – производный класс), а ромбовидными стрелками – отношения ассоциации (класс-владелец – класс-компонент).

![](http://i.imgur.com/h5Mm9EV.png)

В соответствии с предложенной структурой классов модульная (файловая) структура программной системы может иметь вид:

 - DatValue.h – модуль, объявляющий абстрактный класс объектов-значений списка;
 - RootLink.h, RootLink.cpp – модуль базового класса для звеньев (элементов) списка;
 - DatLink.h, DatLink.cpp – модуль класса для звеньев (элементов) списка с указателем на объект-значение;
 - DatList.h, DatList.cpp – модуль класса линейных списков;
 - HeadRing.h, HeadRing.cpp – модуль класса циклических списков с заголовком;
 - Monom.h, Monom.cpp – модуль класса моном;
 - Polinom.h, Polinom.cpp – модуль класса полиномов;
 - test_polinom.cpp, test_tdatlist.cpp – модуль программы тестирования.

## Реализация


##Из-за неудобности решения предложенного в реализации, новые типы данных, обозначающих указатели на объекты(PTMonom, PTDatList и т.д.) решено не объявлять, кроме PTRootLink.

### Файл `DatValue.h`

```c++

#pragma once

//абстрактный класс объектов значений списка
class TDatValue {
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};

```

### Файл `RootLink.h`

```c++

#pragma once

#include "DatValue.h"
//класс должен быть объявлен тут, чтобы сработало объвление typedef

class TRootLink;
typedef TRootLink* PTRootLink;
//Базовый клас для звеньев
class TRootLink {
protected:
	PTRootLink pNext;
public:
	TRootLink(PTRootLink _pNext = nullptr) :pNext(_pNext) {};
	PTRootLink GetNextLink() { return pNext; }
	
	//Setter for pNext
	void SetNextLink(PTRootLink _pNext) { pNext = _pNext; }
	
	void InsNextLink(PTRootLink _pLink) {
		PTRootLink p = pNext;//записываем старое значение
		pNext = _pLink;//меняем его
		if (_pLink != nullptr) _pLink->pNext = p;
	}
	virtual void       SetDatValue(TDatValue* _val ) = 0;
	virtual TDatValue* GetDatValue()                 = 0;

	 friend class TDatList;
};
```

### Файл `DatLink.h`

```c++

#pragma once

#include "RootLink.h"
//класс для звеньев списка с указателем на объект значение
class TDatLink : public TRootLink {
protected: 
	TDatValue *pValue;//указатель на объект значения

public:
	//Constructor
	TDatLink(TDatValue* _pValue = nullptr, PTRootLink _pNext = nullptr)
		:TRootLink(_pNext), pValue(_pValue) {};

	//Setters and Getters for pValue
	void SetDatValue(TDatValue* _pValue) { pValue = _pValue; }
	TDatValue* GetDatValue() { return pValue; }

	TDatLink* GetNextDatLink() { return (TDatLink*)pNext; }

	friend class TDatList;
};

```

### Файл `DatList.h`

```c++
#pragma once

#include "DatLink.h"

enum TLinkPos {BEGIN, CURRENT, END};

//Класс линейных списков
class TDatList  {
protected:
	TDatLink* pFirst; //Первое звено
	TDatLink* pLast;//Последнее звено
	TDatLink* pCurrLink;//Текущее звено
	TDatLink* pPrevLink;//Звено перед текущим
	TDatLink* pStop; //Значение указателя, означающего конец списка

	int CurrPos; //Номер текущей позиции
	int ListLen; //Количество звеньев

//METHODS_________________________________________________________________
		//Добавление звена
	TDatLink* GetLink(TDatValue* pVal = nullptr, TDatLink* pNext = nullptr);
		//Удаление Звена
	void DelLink(TDatLink* pLink);

public:
	TDatList();
	~TDatList() { DelList(); }
	// доступ
	TDatValue* GetDatValue(TLinkPos mode = CURRENT) const; // значение
	virtual bool IsEmpty()  const { return pFirst == pStop; } // список пуст ?
	int GetListLength()    const { return ListLen; }       // к-во звеньев
	// навигация
	int SetCurrentPos(int pos);          // установить текущее звено
	int GetCurrentPos(void) const;       // получить номер тек. звена
	virtual int Reset(void);             // установить на начало списка
	virtual bool IsListEnded(void) const; // список завершен ?
	int GoNext(void);                    // сдвиг вправо текущего звена
										 // (=1 после применения GoNext для последнего звена списка)
	// вставка звеньев
	virtual void InsFirst(TDatValue* pVal = nullptr); // перед первым
	virtual void InsLast(TDatValue* pVal = nullptr); // вставить последним 
	virtual void InsCurrent(TDatValue* pVal = nullptr); // перед текущим 
	// удаление звеньев
	virtual void DelFirst(void);    // удалить первое звено 
	virtual void DelCurrent(void);    // удалить текущее звено 
	virtual void DelList(void);    // удалить весь список



};
```

### Файл `DatList.cpp`

```c++

#include "DatList.h"
#include <math.h>
//Возвращение пустого звена
TDatLink * TDatList::GetLink(TDatValue * _pVal, TDatLink * _pLink)
{
	return new TDatLink(_pVal,_pLink);
}

//Delete value and its links
void TDatList::DelLink(TDatLink * _pLink)
{
	if (_pLink) {
		if (_pLink->pValue != 0) delete _pLink->pValue;//Delete memory of Value
		delete _pLink;//Delete memory of link
	}
}
//DefaultConstructor
TDatList::TDatList():CurrPos(-1), ListLen(0)
{
	pFirst = pLast = pCurrLink = pPrevLink = pStop = nullptr;
}
//Position of First, Last or Current
TDatValue * TDatList::GetDatValue(TLinkPos _mode) const
{
	switch (_mode)
	{
	case BEGIN:
		return pFirst->pValue;
		break;
	case END:
		return pLast->pValue;
		break;
	default:
		return pCurrLink->pValue;
		break;
	}
}
//Set Current Position
int TDatList::SetCurrentPos(int pos)
{
	if (pos <= GetListLength()) {
		Reset();
		for (int i = 0;i < pos;i++, GoNext()) {}
	}
	else
		throw 1;
}
//Get Current Position
int TDatList::GetCurrentPos(void) const
{
	return CurrPos;
}
//Set current position on the begining of List 
int TDatList::Reset(void)
{
	pPrevLink = pStop;//we haven`t object before the First
	pCurrLink = pFirst;
	CurrPos = IsEmpty() ? -1 : 0;//If LIST is empty CerrPos is -1
	return 0;
}
//Is List Ended?
bool TDatList::IsListEnded(void) const
{
	return pCurrLink==pStop;
}

int TDatList::GoNext(void)
{
	if(IsListEnded())
	return 0;
	else {
		CurrPos++;
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		return 1;
	}
}

//INSERT//INSERT//INSERT//INSERT//INSERT//INSERT//INSERT//INSERT
void TDatList::InsFirst(TDatValue * pVal)
{
	TDatLink* buf = GetLink(pVal,pFirst);
	if (buf) {
		pFirst = buf;
		ListLen++;
	}
}

void TDatList::InsLast(TDatValue * pVal)
{
	TDatLink* tmp = GetLink(pVal, pStop);
	if (tmp)//check on nullptr
	{
		if (pLast)
			pLast->SetNextLink(tmp);
		pLast = tmp;
		ListLen++;
		if (ListLen == 1) {
			pFirst = tmp;
			Reset();
		}
		if (IsListEnded())
			pCurrLink = tmp;


	}
}

void TDatList::InsCurrent(TDatValue * pVal)
{
	if ((pCurrLink == pFirst) || IsEmpty())
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else {
		TDatLink* tmp = GetLink(pVal, pCurrLink);
		if (tmp) {
			pPrevLink->SetNextLink(tmp);
			tmp->SetNextLink(pCurrLink);
			ListLen++;
			pCurrLink = tmp;
		}
	}
}
//DELETE//DELETE//DELETE//DELETE//DELETE//DELETE//DELETE//DELETE//DELETE//
void TDatList::DelFirst(void)
{
	if (!IsEmpty()) {
		TDatLink* tmp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(tmp);
		ListLen--;

		if (IsEmpty()) {
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 1)
			pPrevLink = pStop;
		else if (CurrPos == 0)
			pCurrLink = pFirst;
		if (CurrPos)
			CurrPos--;
	}
	else
		throw 111;
}

void TDatList::DelCurrent(void)
{
	if ((pCurrLink)&&(!IsEmpty()))
	{
		if (pCurrLink == pFirst)
			DelFirst();
		else {
			TDatLink* tmp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(tmp);
			ListLen--;
			if (pCurrLink == pLast)
			{
				pLast = pPrevLink;
				pCurrLink = pStop;

			}
		}
	}
	else {
		throw 111;
	}
}

void TDatList::DelList(void)
{
	while (!IsEmpty())
		DelFirst();
	CurrPos = -1;
	pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
}

```

### Файл `HeadRing.h`

```c++

#pragma once

#include "DatList.h"

//Класс циклических заголовков
class THeadRing : public TDatList {
protected:
	TDatLink* pHead; //Заголовок, pFirst - звено за pHead
public:
	THeadRing() :TDatList() {
		InsLast();
		pHead = pFirst;
		pStop = pHead;
		Reset();
		ListLen = 0;
		pFirst->SetNextLink(pFirst);
	}
	~THeadRing() {
		DelList();
		DelLink(pHead);
		pHead = nullptr;
	}

	//Вставка звеньев
	virtual void InsFirst(TDatValue* pVal = nullptr) //После заголовка
	{
		TDatList::InsFirst(pVal);
		pHead->SetNextLink(pFirst);
	}
	virtual void DelFirst(void)//Удалить первое звено
	{
		TDatList::DelFirst();
		pHead->SetNextLink(pFirst);
	}
};

```


### Файл `Monom.h`

```c++
#pragma once
#include "DatValue.h"
#include <iostream>
#include <string>

using namespace std;
//Класс для мономов
class TMonom :public TDatValue {
protected:
	int coeff;//coefficient value
	int index;//index value

public:
	TMonom(int _coeff = 1, int _index = 0) :coeff(_coeff), index(_index)
	{};
	virtual TDatValue * GetCopy();

	//Getters and Setters for coefficient
	void SetCoeff(int _coeff) { coeff = _coeff; }
	int GetCoeff(void) { return coeff; }
	//Getter and Setters for index
	void SetIndex(int _index) { index = _index; }
	int GetIndex(void) { return index; }

	//Operators overloading
	TMonom& operator= (const TMonom &tm) {
		coeff = tm.coeff;
		index = tm.index;
		return *this;
	}
	bool operator==(const TMonom &tm) {
		return (coeff == tm.coeff) && (index == tm.index);
	}
	bool operator<(const TMonom &tm) {
		return index < tm.index;
	}

	double  TMonom::calculate(double x, double y, double z)
	{
		int indx = index / 100;
		int indy = (index % 100) / 10;
		int indz = index % 10;
		return coeff*pow(x, indx)*pow(y, indy)*pow(z, indz);
	}
	bool operator>(const TMonom &tm) {
		return index > tm.index;
	}

	friend ostream& operator<<(ostream &os, TMonom &tm) {
		string str = "";
		if (tm.coeff < 0)
			str += '-';
		else
			str += '+';
		str += to_string(abs(tm.coeff));
		if (tm.index != 0)
		{
			int indx = tm.index / 100;
			int indy = (tm.index % 100) / 10;
			int indz = tm.index % 10;
			if (indx != 0)
				if (indx != 1)
					str += ("*x^" + to_string(indx));
				else
					str += "*x";
			if (indy != 0)
				if (indy != 1)
					str += ("*y^" + to_string(indy));
				else
					str += "*y";
			if (indz != 0)
				if (indz != 1)
					str += ("*z^" + to_string(indz));
				else
					str += "*z";
		}
		os << str << ' ';
		return os;
	}
	friend class TPolinom;
};
```

### Файл `Monom.cpp`

```c++

#include "Monom.h"

TDatValue* TMonom::GetCopy()
{
	
	TMonom *buf = new TMonom(this->coeff,this->index);
	return buf;
}


```

### Файл `Polinom.h`

```c++

#pragma once

#include "Monom.h"
#include "HeadRing.h"


class TPolinom : public THeadRing {
private:
	void update(void);

public:
	TPolinom(int monoms[][2] = nullptr, int km = 0); // конструктор
												  // полинома из массива «коэффициент-индекс»
	TPolinom(TPolinom &q);      // конструктор копирования
	TMonom*  GetMonom() { return (TMonom*)GetDatValue(); }
	TPolinom & operator+(TPolinom &q); // сложение полиномов
	TPolinom & operator=(TPolinom &q); // присваивание
	bool operator==(TPolinom &q); // сравнение
	double calculate(double, double, double);//нахождение значения
	friend ostream & operator<<(ostream & os, TPolinom & q); // перегрузка вывода
};

```

### Файл `Polinom.cpp`

```c++
#pragma once
#include "Polinom.h"


//PRIVATE//PRIVATE//PRIVATE//PRIVATE//PRIVATE//PRIVATE
void TPolinom::update(void)
{//складываем одинаковые индексы и удаляем нулевые коэфиценты
	int pos = -1;
	TMonom* currMon,* polMon;
	TPolinom tempPol(*this);
	for (Reset();!IsListEnded();GoNext()) {
		currMon = (TMonom*)GetDatValue();
		tempPol.Reset();
		while (!tempPol.IsListEnded())
		{
			if (GetCurrentPos() != tempPol.GetCurrentPos())
			{//If reset() change CurrentPos
				polMon = (TMonom*)tempPol.GetDatValue();
				if (currMon->index == polMon->index&& currMon->index != -1) {
				// If they index are equal
					pos = GetCurrentPos();
					currMon->coeff += polMon->coeff;

					SetCurrentPos(tempPol.GetCurrentPos());
					DelCurrent();
					tempPol.DelCurrent();
					SetCurrentPos(pos);
					tempPol.Reset();
				}
			}
			tempPol.GoNext();
			}
	}
		for (Reset();!IsListEnded();GoNext()) 
			if (((TMonom*)GetDatValue())->coeff == 0)
				DelCurrent();
		Reset();

}

//CONSTRUSTORS//CONSTRUSTORS//CONSTRUSTORS//CONSTRUSTORS//CONSTRUSTORS//CONSTRUSTORS//CONSTRUSTORS

TPolinom::TPolinom(int monoms[][2], int km) {
	TMonom *elem = new TMonom(0, -1);
	pHead->SetDatValue(elem);
	for (int i = 0; i < km; i++)
	{
		elem = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(elem);
	}
	update();
}

TPolinom::TPolinom(TPolinom &q) {
	TMonom* elem = new TMonom(0, -1);
	pHead->SetDatValue(elem);
	int tempPosition = q.GetCurrentPos();
	for (q.Reset();!q.IsListEnded();q.GoNext()) {
		elem = q.GetMonom();
		InsLast(elem->GetCopy());
	}
	q.SetCurrentPos(tempPosition);


}


//OPERATORS//OPERATORS//OPERATORS//OPERATORS//OPERATORS//OPERATORS//OPERATORS//OPERATORS

TPolinom& TPolinom::operator+(TPolinom &q)
{
	TMonom* this_mon, *q_mon, *new_mon;
	q.Reset();
	Reset();
	TPolinom* result = new TPolinom(*this);

	while (true)
	{
		this_mon = result->GetMonom();
		q_mon = q.GetMonom();
		if ((this_mon->operator< (*q_mon)))
		{
			new_mon = new TMonom(q_mon->coeff, q_mon->index);
			result->InsCurrent(new_mon);
			q.GoNext();
		}else if (q_mon->operator<(*this_mon))
			result->GoNext();
		else
		{
			if (this_mon->index == -1)
				break;
			this_mon->coeff += q_mon->coeff;
			if (this_mon->coeff != 0) {
				result->GoNext();
				q.GoNext();
			}
			else {
				result->DelCurrent();
				q.GoNext();
			}
		}



	}
	result->update();
	return *result;

}

TPolinom& TPolinom::operator=(TPolinom &q)
{
	if ((&q) && (&q != this))//if q is correct and protected from yourself prisvaivanie
	{
		DelList();
		TMonom* Mon = new TMonom(0, -1);
		pHead->SetDatValue(Mon);
		int tempPosition = q.GetCurrentPos();
		for (q.Reset();!q.IsListEnded();q.GoNext())
		{
			Mon = q.GetMonom();
			InsLast(Mon->GetCopy());

		}
		q.SetCurrentPos(tempPosition);

	}
	return *this;
}

bool TPolinom:: operator==(TPolinom &q)
{
	bool result = true;
	bool tempCheck = false;
	int tempPositionLeft = GetCurrentPos();
	int tempPositionRight = q.GetCurrentPos();
	if (q.GetListLength() == GetListLength())
		for (Reset();!IsListEnded();GoNext())
		{
			for (q.Reset(); !q.IsListEnded();q.GoNext())
			{
				tempCheck = false;
				if (((TMonom*)GetDatValue())->operator==(*(TMonom*)(q.GetDatValue())))
				{
					tempCheck = true;
					break;
				}
			}
			if (!tempCheck)
				break;
		}
	result = tempCheck;
	if (q.GetListLength() == 0 && GetListLength() == 0)
		result = true;
	SetCurrentPos(tempPositionLeft);
	q.SetCurrentPos(tempPositionRight);
	return result;

}

double TPolinom::calculate(double x, double y, double z)
{
	double result = 0;
	if (ListLen != 0)
		for (Reset(); !IsListEnded(); GoNext())
			result += ((TMonom*)GetDatValue())->calculate(x, y, z);
	return result;
}
ostream & operator<<(ostream & os, TPolinom & q) {
	if (q.ListLen != 0)
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
			cout << (TMonom)(*q.GetMonom());
	else
		cout << "Polinom is empty";
	return os;
}

```

### Файл `test_tdatlist.cpp`

```c++

#pragma once

#include "gtest.h"
#include "DatList.h"

typedef struct ValueInt :TDatValue
{
	int Value;
	ValueInt() { Value = 0; }
	virtual TDatValue * GetCopy() { TDatValue *temp = new ValueInt; return temp; }
} *PValueInt;

//Initialization//////////////////////////////////////////////////////////////////
class TestDatList : public ::testing::Test
{
protected:
	virtual void SetUp()
	{
		// Testing only elem
		Val = new ValueInt();
		Val->Value = 100;

		// Testing ten elems
		ArrVal = new PValueInt[N];
		for (int i = 0; i < N; i++)
		{
			ArrVal[i] = new ValueInt();
			ArrVal[i]->Value = i;
		}
	}
	virtual void TearDown()
	{
		delete ArrVal;
	}
	const int N = 10;
	PValueInt Val;
	PValueInt *ArrVal;
	TDatList List;
};

TEST_F(TestDatList, new_list_has_null_size)
{
	// NO ACTIONS

	EXPECT_EQ(List.GetListLength(), 0);
}

TEST_F(TestDatList, reset_makes_current_position_equil_null)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.Reset();

	EXPECT_EQ(List.GetCurrentPos(), 0);
}

TEST_F(TestDatList, setcurrentpos_changes_the_current_position)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.SetCurrentPos(5);

	EXPECT_EQ(List.GetCurrentPos(), 5);
}

TEST_F(TestDatList, setcurrentpos_can_change_position_if_it_is_more_than_size_of_list)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);

	EXPECT_ANY_THROW(List.SetCurrentPos(12));
}

TEST_F(TestDatList, size_of_the_list_is_correct_after_inserting)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);

	EXPECT_EQ(List.GetListLength(), N);
}

TEST_F(TestDatList, size_of_the_list_is_correct_after_N_times_removing)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	for (int i = 0; i < N; i++)
		List.DelFirst();

	EXPECT_EQ(List.GetListLength(), 0);
}

TEST_F(TestDatList, size_of_the_list_is_correct_after_removing_of_all_list)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.DelList();

	EXPECT_EQ(List.GetListLength(), 0);
}

TEST_F(TestDatList, cant_delete_elem_if_size_equil_null)
{
	// NO ACTIONS

	EXPECT_ANY_THROW(List.DelFirst());
	EXPECT_ANY_THROW(List.DelCurrent());
}

TEST_F(TestDatList, list_is_ended_if_current_position_equil_length_of_list)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.Reset();
	List.SetCurrentPos(List.GetListLength());

	EXPECT_TRUE(List.IsListEnded());
}


TEST_F(TestDatList, can_add_an_item_to_the_end)
{
	for (int i = 0; i < N - 1; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8
	List.InsLast(Val);
	// 0 1 2 3 4 5 6 7 8 100
	List.SetCurrentPos(List.GetListLength()-1);

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value,Val->Value);
}

TEST_F(TestDatList, can_add_an_item_to_the_beginning)
{
	for (int i = 0; i < N - 1; i++)
		List.InsFirst(ArrVal[i]);
	// 8 7 6 5 4 3 2 1 0
	List.InsFirst(Val);
	// 100 8 7 6 5 4 3 2 1 0

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value, Val->Value);
}

TEST_F(TestDatList, can_add_an_item_to_the_current_position)
{
	for (int i = 0; i < N-1; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8
	// Current = 0
	List.SetCurrentPos(5);
	// Current = 5
	List.InsCurrent(Val);
	// 0 1 2 3 4 100 5 6 7 8

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value, Val->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_end)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9

	for (int i = 0; i < N; i++, List.GoNext())
		EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_beginnig)
{
	for (int i = 0; i < N; i++)
		List.InsFirst(ArrVal[i]);
	// 9 8 7 6 5 4 3 2 1 0

	for (int i = N - 1; i > 0; i--, List.GoNext())
		EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_current_pos)
{
	for (int i = 0; i < 5; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4
	// Current = 0
	List.GoNext();
	List.GoNext();
	// Current = 2
	List.InsCurrent(Val);
	// 0 1 100 2 3 4

	List.Reset();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[0]->Value);
	List.GoNext();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[1]->Value);
	List.GoNext();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, Val->Value);
	List.GoNext();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
}

TEST_F(TestDatList, can_delete_elem_from_beginnig)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9
	List.DelFirst();
	// 1 2 3 4 5 6 7 8 9

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value, ArrVal[1]->Value);
	EXPECT_EQ(List.GetListLength(), N - 1);
}

TEST_F(TestDatList, can_delete_elem_from_the_current_pos)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9
	List.GoNext();
	List.DelCurrent();
	// 0 2 3 4 5 6 7 8 9

	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
	EXPECT_EQ(List.GetListLength(), N - 1);
}

```

![](http://i.imgur.com/oua4uvI.png)

### Файл `test_polinom.cpp`

```c++

#pragma once

#include "gtest.h"
#include "DatList.h"
#include "Polinom.h"

TEST(TPolinom, can_create_polinom_without_parametres)
{
	// NO ACTIONS

	EXPECT_NO_THROW(TPolinom Pol);
}

TEST(TPolinom, polinom_without_parametres_has_its_own_adress)
{
	TPolinom Pol;

	EXPECT_NE(&Pol,nullptr);
}

TEST(TPolinom, polinom_copied_by_constructer_is_equal_to_origin)
{
	const int size = 2;
	int mon[][2] = { { 5, 3 },{ 2, 4 } };
	TPolinom Pol1(mon, size);

	TPolinom Pol2 = Pol1;

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, polinom_copied_by_constructer_has_its_own_adress)
{
	const int size = 2;
	int mon[][2] = { { 5, 3 },{ 2, 4 } };
	TPolinom Pol1(mon, size);

	TPolinom Pol2 = Pol1;

	EXPECT_NE(&Pol1,&Pol2);
}

TEST(TPolinom, can_compare_the_polynoms)
{
	const int size = 3;
	int mon[][2] = { { 1, 3 },{ 2, 4 },{ 2, 100 } };
	TPolinom Pol1(mon, size);
	TPolinom Pol2(mon, size);

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_assign_polynoms_and_they_are_equal)
{
	// Arrange
	const int size = 2;
	int mon[][2] = { { 5, 3 },{ 2, 4 } };
	TPolinom Pol1(mon, size);
	TPolinom Pol2;

	Pol2 = Pol1;

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_assign_many_polynoms_and_they_are_equal)
{
	const int size = 2;
	int mon[][2] = { { 5, 3 },{ 2, 4 } };
	TPolinom Pol1(mon, size);
	TPolinom Pol2,Pol3;

	Pol3=Pol2 = Pol1;
	bool _check = (Pol1 == Pol2) && (Pol2 == Pol3) && (Pol1 == Pol3);

	EXPECT_TRUE(_check);
}

TEST(TPolinom, can_add_up_simple_polynoms)
{
	const int size1 = 3;
	const int size2 = 4;
	int mon1[][2] = { { 5, 2 },{ 8, 3 },{ 9, 4 } };
	int mon2[][2] = { { 1, 1 },{ -8, 3 },{ 1, 4 },{ 2, 5 } };
	// 5z^2+8z^3+9z^4
	TPolinom Pol1(mon1, size1);
	// z-8z^3+z^4+2z^5
	TPolinom Pol2(mon2, size2);

	TPolinom Pol= Pol1 + Pol2;

	const int expected_size = 4;
	int expected_mon[][2] = { { 1, 1 },{ 5, 2 },{ 10, 4 },{ 2, 5 } };
	// z+5z^2+10z^4+2z^5
	TPolinom check_Pol(expected_mon, expected_size);

	EXPECT_TRUE(Pol == check_Pol);
}

TEST(TPolinom, can_create_polinom_from_one_monom)
{
	int mon[][2] = { { 1, 3 } };

	TPolinom A(mon, 1);

	TMonom res(1, 3);
	EXPECT_EQ(res, (TMonom)(*A.GetMonom()));
}

TEST(TPolinom, can_create_polinom_from_two_monoms)
{
	const int size = 2;
	int mon[][2] = { { 1, 3 },{ 2, 4 } };

	TPolinom Pol(mon, size);

	TMonom monoms[size];
	for (int i = 0; i < size; i++)
		monoms[i] = TMonom(mon[i][0], mon[i][1]);
	for (int i = 0; i < size; i++, Pol.GoNext())
		EXPECT_EQ(monoms[i], *Pol.GetMonom());
}

TEST(TPolinom, can_create_pol_from_many_monoms)
{
	const int size = 10;
	int mon[][2] = { { 1, 3 },{ 2, 4 },{ 2, 100 },{ 3, 110 },
	{ 5, 150 },{ 6, 302 },{ 3, 400 },{ 2, 500 },{ 7 ,800 },{ 2, 888 } };

	TPolinom Pol(mon, size);

	TMonom monoms[size];
	for (int i = 0; i < size; i++)
		monoms[i] = TMonom(mon[i][0], mon[i][1]);
	for (int i = 0; i < size; i++, Pol.GoNext())
		EXPECT_EQ(monoms[i], *Pol.GetMonom());
}

TEST(TPolinom, can_assign_empty_polynom)
{
	TPolinom Pol1;
	TPolinom Pol2;

	Pol2 = Pol1;

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_add_up_linear_polynoms)
{
	const int size = 1;
	int mon1[][2] = { { 2, 1 } };
	int mon2[][2] = { { 1, 1 } };
	// 2z
	TPolinom Pol1(mon1, size);
	// z
	TPolinom Pol2(mon2, size);

	TPolinom Pol = Pol1 + Pol2;

	const int expected_size = 1;
	int expected_mon[][2] = { { 3, 1 } };
	// z+2z^2
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_add_up_polynoms)
{
	const int size1 = 5;
	const int size2 = 4;
	int mon1[][2] = { { 5, 213 },{ 8, 321 },{ 10, 432 },{ -21, 500 },{ 10, 999 } };
	int mon2[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
	// 5x^2yz^3+8x^3y^2z+10x^4y^3z^2-21x^5+10x^9y^9z^9
	TPolinom Pol1(mon1, size1);
	// 15-8x^3y^2z+x^5+20x^7z^2
	TPolinom Pol2(mon2, size2);

	TPolinom Pol = Pol1 + Pol2;

	const int expected_size = 6;
	int expected_mon[][2] = { { 15, 0 },{ 5, 213 },{ 10, 432 },{ -20, 500 },{ 20, 702 },{ 10, 999 } };
	// 15+5x^2yz^3+10x^4y^3z^2-20x^5+20x^7z^2+10x^9y^9z^9
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_add_up_many_polynoms)
{
	const int size1 = 3;
	const int size2 = 4;
	const int size3 = 3;
	int mon1[][2] = { { 5, 2 },{ 8, 3 },{ 9, 4 } };
	int mon2[][2] = { { 1, 1 },{ -8, 3 },{ 1, 4 },{ 2, 5 } };
	int mon3[][2] = { { 10, 0 },{ 2, 3 },{ 8, 5 } };
	// 5z^2+8z^3+9z^4
	TPolinom Pol1(mon1, size1);
	// z-8z^3+z^4+2z^5
	TPolinom Pol2(mon2, size2);
	// 10+2z^3+8z^5
	TPolinom Pol3(mon3, size3);

	TPolinom Pol = Pol1 + Pol2 + Pol3;

	const int expected_size = 6;
	int expected_mon[][2] = { { 10, 0 },{ 1, 1 },{ 5, 2 },{ 2, 3 },{ 10, 4 },{ 10, 5 } };
	// z+5z^2+10z^4+2z^5
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_calculate_the_polinom_with_different_monoms)
{
	int size = 3;
	int mon[][2] = { { 1, 1 },{ 2, 10 },{ 3, 100 } };
	TPolinom Pol(mon, size);

	EXPECT_EQ(Pol.calculate(1, 1, 1), 6);
}

TEST(TPolinom, can_calculate_the_polinom_with_equal_monoms_with_different_coeffs)
{
	int size = 3;
	int mon[][2] = { { 1, 111 },{ -2, 111 },{ 3, 111 } };
	TPolinom Pol(mon, size);

	EXPECT_EQ(Pol.calculate(1, 1, 1), 2);
}

TEST(TPolinom, can_calculate_the_polinom_with_different_and_equals_monoms)
{
	int size = 3;
	int mon[][2] = { { 1, 111 },{ -1, 111 },{ 2, 11 } };
	TPolinom Pol(mon, size);

	EXPECT_EQ(Pol.calculate(1, 1, 1), 2);
}

TEST(TPolinom, can_calculate_the_empty_polinom_and_it_is_equal_null)
{
	TPolinom Pol;

	EXPECT_EQ(Pol.calculate(1, 1, 1), 0);
}

```

![](http://i.imgur.com/FCjofUo.png)

## Демонстрация сложения и подсчета значения

```c++

#include "Polinom.h"

int main()
{
	#include <stdio.h>
#include "Polinom.h"


int main() {
	int a[5][2]{ { 1,111 },{ 2,222 },{ 1,345 },{ 1,1 },{ 0,888 } };
	int b[7][2]{ { 1,111 },{ 2,222 },{ 1,346 },{ 7,1 },{ 7,000 }, {1,555 },{4,346} };
	TPolinom first(a, 5);
	TPolinom second(b, 7);
	cout << first << endl;
	cout << second << endl;

	TPolinom third = first + second;

	cout << third << endl;


	return 0;
}
}

```

## Вывод:
 - данная реализация найдена очень неудобной, громоздкой и непонятной, для упрощения работы, более легкого понимания и упрощения структуры предлагается идея использовать шаблоны.
 - в зависимости от выбора используемых структур хранения, сложность алгоритмов может возрастать или уменьшаться.


