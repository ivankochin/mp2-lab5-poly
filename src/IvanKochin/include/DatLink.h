#pragma once

#include "RootLink.h"
//����� ��� ������� ������ � ���������� �� ������ ��������
class TDatLink : public TRootLink {
protected: 
	TDatValue *pValue;//��������� �� ������ ��������

public:
	//Constructor
	TDatLink(TDatValue* _pValue = nullptr, PTRootLink _pNext = nullptr)
		:TRootLink(_pNext), pValue(_pValue) {};

	//Setters and Getters for pValue
	void SetDatValue(TDatValue* _pValue) { pValue = _pValue; }
	TDatValue* GetDatValue() { return pValue; }

	TDatLink* GetNextDatLink() { return (TDatLink*)pNext; }

	friend class TDatList;
};