#pragma once

#include "DatValue.h"
//����� ������ ���� �������� ���, ����� ��������� ��������� typedef

class TRootLink;
typedef TRootLink* PTRootLink;
//������� ���� ��� �������
class TRootLink {
protected:
	PTRootLink pNext;
public:
	TRootLink(PTRootLink _pNext = nullptr) :pNext(_pNext) {};
	PTRootLink GetNextLink() { return pNext; }
	
	//Setter for pNext
	void SetNextLink(PTRootLink _pNext) { pNext = _pNext; }
	
	void InsNextLink(PTRootLink _pLink) {
		PTRootLink p = pNext;//���������� ������ ��������
		pNext = _pLink;//������ ���
		if (_pLink != nullptr) _pLink->pNext = p;
	}
	virtual void       SetDatValue(TDatValue* _val ) = 0;
	virtual TDatValue* GetDatValue()                 = 0;

	 friend class TDatList;
};