#pragma once

#include "DatList.h"

//����� ����������� ����������
class THeadRing : public TDatList {
protected:
	TDatLink* pHead; //���������, pFirst - ����� �� pHead
public:
	THeadRing() :TDatList() {
		InsLast();
		pHead = pFirst;
		pStop = pHead;
		Reset();
		ListLen = 0;
		pFirst->SetNextLink(pFirst);
	}
	~THeadRing() {
		DelList();
		DelLink(pHead);
		pHead = nullptr;
	}

	//������� �������
	virtual void InsFirst(TDatValue* pVal = nullptr) //����� ���������
	{
		TDatList::InsFirst(pVal);
		pHead->SetNextLink(pFirst);
	}
	virtual void DelFirst(void)//������� ������ �����
	{
		TDatList::DelFirst();
		pHead->SetNextLink(pFirst);
	}
};