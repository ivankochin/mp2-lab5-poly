#pragma once

#include "Monom.h"
#include "HeadRing.h"


class TPolinom : public THeadRing {
private:
	void update(void);

public:
	TPolinom(int monoms[][2] = nullptr, int km = 0); // �����������
												  // �������� �� ������� ������������-������
	TPolinom(TPolinom &q);      // ����������� �����������
	TMonom*  GetMonom() { return (TMonom*)GetDatValue(); }
	TPolinom & operator+(TPolinom &q); // �������� ���������
	TPolinom & operator=(TPolinom &q); // ������������
	bool operator==(TPolinom &q); // ���������
	double calculate(double, double, double);//���������� ��������
	friend ostream & operator<<(ostream & os, TPolinom & q); // ���������� ������
};
