#pragma once
#include "DatValue.h"
#include <iostream>
#include <string>

using namespace std;
//����� ��� �������
class TMonom :public TDatValue {
protected:
	int coeff;//coefficient value
	int index;//index value

public:
	TMonom(int _coeff = 1, int _index = 0) :coeff(_coeff), index(_index)
	{};
	virtual TDatValue * GetCopy();

	//Getters and Setters for coefficient
	void SetCoeff(int _coeff) { coeff = _coeff; }
	int GetCoeff(void) { return coeff; }
	//Getter and Setters for index
	void SetIndex(int _index) { index = _index; }
	int GetIndex(void) { return index; }

	//Operators overloading
	TMonom& operator= (const TMonom &tm) {
		coeff = tm.coeff;
		index = tm.index;
		return *this;
	}
	bool operator==(const TMonom &tm) {
		return (coeff == tm.coeff) && (index == tm.index);
	}
	bool operator<(const TMonom &tm) {
		return index < tm.index;
	}

	double  TMonom::calculate(double x, double y, double z)
	{
		int indx = index / 100;
		int indy = (index % 100) / 10;
		int indz = index % 10;
		return coeff*pow(x, indx)*pow(y, indy)*pow(z, indz);
	}
	bool operator>(const TMonom &tm) {
		return index > tm.index;
	}

	friend ostream& operator<<(ostream &os, TMonom &tm) {
		string str = "";
		if (tm.coeff < 0)
			str += '-';
		else
			str += '+';
		str += to_string(abs(tm.coeff));
		if (tm.index != 0)
		{
			int indx = tm.index / 100;
			int indy = (tm.index % 100) / 10;
			int indz = tm.index % 10;
			if (indx != 0)
				if (indx != 1)
					str += ("*x^" + to_string(indx));
				else
					str += "*x";
			if (indy != 0)
				if (indy != 1)
					str += ("*y^" + to_string(indy));
				else
					str += "*y";
			if (indz != 0)
				if (indz != 1)
					str += ("*z^" + to_string(indz));
				else
					str += "*z";
		}
		os << str << ' ';
		return os;
	}
	friend class TPolinom;
};