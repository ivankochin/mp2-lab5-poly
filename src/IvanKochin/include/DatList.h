#pragma once

#include "DatLink.h"

enum TLinkPos {BEGIN, CURRENT, END};

//����� �������� �������
class TDatList  {
protected:
	TDatLink* pFirst; //������ �����
	TDatLink* pLast;//��������� �����
	TDatLink* pCurrLink;//������� �����
	TDatLink* pPrevLink;//����� ����� �������
	TDatLink* pStop; //�������� ���������, ����������� ����� ������

	int CurrPos; //����� ������� �������
	int ListLen; //���������� �������

//METHODS_________________________________________________________________
		//���������� �����
	TDatLink* GetLink(TDatValue* pVal = nullptr, TDatLink* pNext = nullptr);
		//�������� �����
	void DelLink(TDatLink* pLink);

public:
	TDatList();
	~TDatList() { DelList(); }
	// ������
	TDatValue* GetDatValue(TLinkPos mode = CURRENT) const; // ��������
	virtual bool IsEmpty()  const { return pFirst == pStop; } // ������ ���� ?
	int GetListLength()    const { return ListLen; }       // �-�� �������
	// ���������
	int SetCurrentPos(int pos);          // ���������� ������� �����
	int GetCurrentPos(void) const;       // �������� ����� ���. �����
	virtual int Reset(void);             // ���������� �� ������ ������
	virtual bool IsListEnded(void) const; // ������ �������� ?
	int GoNext(void);                    // ����� ������ �������� �����
										 // (=1 ����� ���������� GoNext ��� ���������� ����� ������)
	// ������� �������
	virtual void InsFirst(TDatValue* pVal = nullptr); // ����� ������
	virtual void InsLast(TDatValue* pVal = nullptr); // �������� ��������� 
	virtual void InsCurrent(TDatValue* pVal = nullptr); // ����� ������� 
	// �������� �������
	virtual void DelFirst(void);    // ������� ������ ����� 
	virtual void DelCurrent(void);    // ������� ������� ����� 
	virtual void DelList(void);    // ������� ���� ������



};