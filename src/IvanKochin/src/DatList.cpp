#include "DatList.h"
#include <math.h>
//����������� ������� �����
TDatLink * TDatList::GetLink(TDatValue * _pVal, TDatLink * _pLink)
{
	return new TDatLink(_pVal,_pLink);
}

//Delete value and its links
void TDatList::DelLink(TDatLink * _pLink)
{
	if (_pLink) {
		if (_pLink->pValue != 0) delete _pLink->pValue;//Delete memory of Value
		delete _pLink;//Delete memory of link
	}
}
//DefaultConstructor
TDatList::TDatList():CurrPos(-1), ListLen(0)
{
	pFirst = pLast = pCurrLink = pPrevLink = pStop = nullptr;
}
//Position of First, Last or Current
TDatValue * TDatList::GetDatValue(TLinkPos _mode) const
{
	switch (_mode)
	{
	case BEGIN:
		return pFirst->pValue;
		break;
	case END:
		return pLast->pValue;
		break;
	default:
		return pCurrLink->pValue;
		break;
	}
}
//Set Current Position
int TDatList::SetCurrentPos(int pos)
{
	if (pos <= GetListLength()) {
		Reset();
		for (int i = 0;i < pos;i++, GoNext()) {}
	}
	else
		throw 1;
}
//Get Current Position
int TDatList::GetCurrentPos(void) const
{
	return CurrPos;
}
//Set current position on the begining of List 
int TDatList::Reset(void)
{
	pPrevLink = pStop;//we haven`t object before the First
	pCurrLink = pFirst;
	CurrPos = IsEmpty() ? -1 : 0;//If LIST is empty CerrPos is -1
	return 0;
}
//Is List Ended?
bool TDatList::IsListEnded(void) const
{
	return pCurrLink==pStop;
}

int TDatList::GoNext(void)
{
	if(IsListEnded())
	return 0;
	else {
		CurrPos++;
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		return 1;
	}
}

//INSERT//INSERT//INSERT//INSERT//INSERT//INSERT//INSERT//INSERT
void TDatList::InsFirst(TDatValue * pVal)
{
	TDatLink* buf = GetLink(pVal,pFirst);
	if (buf) {
		pFirst = buf;
		ListLen++;
	}
}

void TDatList::InsLast(TDatValue * pVal)
{
	TDatLink* tmp = GetLink(pVal, pStop);
	if (tmp)//check on nullptr
	{
		if (pLast)
			pLast->SetNextLink(tmp);
		pLast = tmp;
		ListLen++;
		if (ListLen == 1) {
			pFirst = tmp;
			Reset();
		}
		if (IsListEnded())
			pCurrLink = tmp;


	}
}

void TDatList::InsCurrent(TDatValue * pVal)
{
	if ((pCurrLink == pFirst) || IsEmpty())
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else {
		TDatLink* tmp = GetLink(pVal, pCurrLink);
		if (tmp) {
			pPrevLink->SetNextLink(tmp);
			tmp->SetNextLink(pCurrLink);
			ListLen++;
			pCurrLink = tmp;
		}
	}
}
//DELETE//DELETE//DELETE//DELETE//DELETE//DELETE//DELETE//DELETE//DELETE//
void TDatList::DelFirst(void)
{
	if (!IsEmpty()) {
		TDatLink* tmp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(tmp);
		ListLen--;

		if (IsEmpty()) {
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 1)
			pPrevLink = pStop;
		else if (CurrPos == 0)
			pCurrLink = pFirst;
		if (CurrPos)
			CurrPos--;
	}
	else
		throw 111;
}

void TDatList::DelCurrent(void)
{
	if ((pCurrLink)&&(!IsEmpty()))
	{
		if (pCurrLink == pFirst)
			DelFirst();
		else {
			TDatLink* tmp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(tmp);
			ListLen--;
			if (pCurrLink == pLast)
			{
				pLast = pPrevLink;
				pCurrLink = pStop;

			}
		}
	}
	else {
		throw 111;
	}
}

void TDatList::DelList(void)
{
	while (!IsEmpty())
		DelFirst();
	CurrPos = -1;
	pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
}
