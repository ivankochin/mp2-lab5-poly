#include <stdio.h>
#include "Polinom.h"


int main() {
	int a[5][2]{ { 1,111 },{ 2,222 },{ 1,345 },{ 1,1 },{ 0,888 } };
	int b[7][2]{ { 1,111 },{ 2,222 },{ 1,346 },{ 7,1 },{ 7,000 }, {1,555 },{4,346} };
	TPolinom first(a, 5);
	TPolinom second(b, 7);
	cout << first << endl;
	cout << second << endl;

	TPolinom third = first + second;

	cout << third << endl;



	system("pause");
	return 0;
}


